extends Node

const FILE_LIST_PATH = "user://list.save"
var tree_list = {}


func _ready():
	pass
	tree_list.clear()
	tree_list["tesssst"] = "test_image.png"
	tree_list["tesssst2222"] = "aabb.png"
	save_list()
#	load_list()
#	for t in tree_list.keys():
#		print(t, " ", tree_list[t])


func save_list():
	var savegame = File.new()
	savegame.open(FILE_LIST_PATH, File.WRITE)
	savegame.store_line(to_json(tree_list))
	savegame.close()


func load_list():
	tree_list.clear()
	var save_file = File.new()
	if !save_file.file_exists(FILE_LIST_PATH):
		save_list()
		return null
	save_file.open(FILE_LIST_PATH, File.READ)
	while not save_file.eof_reached():
		var current_line = parse_json(save_file.get_line())
		if current_line != null:
			for k in current_line.keys():
				tree_list[k] = current_line[k]
	return tree_list


func save_tree(tree_name, tree_data):
	tree_list[tree_name] = tree_name + ".png"
	save_list()
	var tree_file_path = "user://" + tree_name + ".tree"

	var savegame = File.new()
	savegame.open(tree_file_path, File.WRITE)
	savegame.store_line(to_json(tree_data))
	savegame.close()


func load_tree(tree_name):
	if not tree_name in tree_list.keys():
		return null
	var tree_file_path = "user://" + tree_name + ".tree"
	
	var save_file = File.new()
	if !save_file.file_exists(tree_file_path):
		return null
		
	save_file.open(tree_file_path, File.READ)
	while not save_file.eof_reached():
		var current_line = parse_json(save_file.get_line())
		if current_line != null:
			print(current_line.keys()[0], " ", current_line.values()[0])

