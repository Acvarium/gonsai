extends Spatial

export(NodePath) var treePath
export (Array, String) var tree_names
var tree 
onready var main_camera = $Camera
var mouse_pressed = false
var rotation_speed = 3.0
var number_of_leafs = 500
var tree_radius = 1.0
var leaf_obj = preload("res://objects/Leaf.tscn")
var cut_effect_obj = preload("res://objects/CutParticleEffect.tscn")
var gallery_item_obj = preload("res://objects/UI/LoadCard.tscn")


var selected_tree_index = 1

var leafControlPoints = []
var tool_mode = 0
var leafUnderCursor = null
var w_speed = 0.1
var corretc_cut_tool_pos = false
var hovered_bone = null
var rot_axis_index = 0
var mouse_over_tools = false
var transp_button_color = Color(1,1,1,0.3)
var menu_shown = true
onready var cam_rest_pos = $Camera.global_transform.origin


func _ready():
	gen_tree_start()
	tool_button_pressed(1)
	#$Effects/bf/AnimationPlayer.play("bf1")


func show_load_screen(to_show):
	if !$UI/Menu.visible:
		menu_shown = to_show
	$UI/LoadScreen.visible = to_show


func move_cam(value):
	var current_pos = $Camera.global_transform.origin
	current_pos.z += value
#	current_pos.y = cam_rest_pos.y - cam_rest_pos.z - current_pos.z
	if cam_rest_pos.distance_to(current_pos) < 3.0:
		$Camera.global_transform.origin = current_pos

func generate_leafs(control_point, leaf_number):
	var max_dist = control_point.global_transform.basis.x.length() + 0.1
	for i in range(leaf_number):
		var x = randf() - 0.5 
		var y = randf() - 0.5
		var z = randf() - 0.5
		var t_pos = Vector3(x, y, z).normalized() * max_dist * randf()
		var leaf = leaf_obj.instance()
		var s = (randf() * 0.8 + 1.5) / max_dist
		leaf.transform = leaf.transform.scaled(Vector3(s, s, s))
		control_point.add_child(leaf)
		
		leaf.global_transform.origin = control_point.global_transform.origin + t_pos

func show_main_menu(to_show):
	$UI/Menu.visible = to_show
	menu_shown = to_show
	

func gen_tree_start():
	show_load_screen(true)
	$Timers/GenStartTimer.start()
	

func generate_tree():
	show_load_screen(true)
	selected_tree_index = randi() % tree_names.size()
	if tree:
		if tree.get_ref():
			tree.get_ref().queue_free()
	var tree_path = "res://models/tree1/" + tree_names[selected_tree_index] + ".tscn"
	tree = weakref(load(tree_path).instance())
	$TreeContainer.add_child(tree.get_ref())
	cut_leafs(true)
	tree.get_ref().reset_pose()
	var cam_pos = cam_rest_pos
	cam_pos.z += tree.get_ref().camera_offset
	$Camera.global_transform.origin = cam_pos
	
	randomize()
	leafControlPoints = []
	for a in tree.get_ref().leafControlPaths:
		for c in tree.get_ref().get_node(a).get_children():
			leafControlPoints.append(c)
			var leaf_num = 100
			if c.has_method("get_leafs_num"):
				leaf_num = c.get_leafs_num()
			generate_leafs(c, leaf_num)
	$Timers/GenStopTimer.start()
	

func _physics_process(delta):
	var rot_dir = 0
	if !menu_shown:
		if Input.is_action_pressed("ui_left"):
			rot_dir = -1
		if Input.is_action_pressed("ui_right"):
			rot_dir = 1
		$TreeContainer.transform = $TreeContainer.transform.rotated(Vector3.UP, \
			delta * rot_dir * rotation_speed)
		if Input.is_action_pressed("c_back"):
			move_cam(2.0 * delta)
		if Input.is_action_pressed("c_forw"):
			move_cam(-2.0 * delta)


func hit_cursor(mouse_pos):
	point_raycast($MouseRay, mouse_pos)
	if $MouseRay.get_collider():
		$CutTool.global_transform.origin = $MouseRay.get_collision_point()
	

func move_cursor(mouse_pos):
	point_raycast($MouseRay, mouse_pos)
	if $MouseRay.get_collider():
		if corretc_cut_tool_pos:
			var cutToolOrigin = $CutTool.global_transform.origin
			cutToolOrigin.z = $MouseRay.get_collision_point().z
			$CutTool.global_transform.origin = cutToolOrigin
			corretc_cut_tool_pos = false
		$CutTool.global_transform = \
			$CutTool.global_transform.looking_at($MouseRay.get_collision_point(), Vector3.UP)
		var m_dist = $CutTool.global_transform.origin.distance_to(\
			 $MouseRay.get_collision_point())
		$CutTool/Area1/CollisionShape.get_shape().height = m_dist
		var col_pos = $CutTool/Area1/CollisionShape.transform.origin
		col_pos.z = -m_dist / 2
		$CutTool/Area1/CollisionShape.transform.origin = col_pos
		$CutTool/Mesh.transform.origin = col_pos
		$CutTool/Mesh.mesh.mid_height = m_dist


func point_raycast(_raycast, mouse_pos):
	_raycast.global_transform.origin = main_camera.global_transform.origin
	var _from = main_camera.project_ray_origin(mouse_pos)
	var _to = _from + main_camera.project_ray_normal(mouse_pos) * 1000
	_raycast.cast_to = _to
	_raycast.force_raycast_update()


func bone_select_move_cursor(mouse_pos):
	point_raycast($MouseRay3, mouse_pos)
	if $MouseRay3.get_collider() != null:
		if $MouseRay3.get_collider().name == "bStatic":
			var current_bone_selector = $MouseRay3.get_collider().get_parent()
			var changed_selection = false
			if hovered_bone:
				if hovered_bone.get_ref():
					if hovered_bone.get_ref() != current_bone_selector:
						hovered_bone.get_ref().highlight(false)
						current_bone_selector.highlight(true)
						changed_selection = true
						hovered_bone = weakref(current_bone_selector)
			if !changed_selection:
				current_bone_selector.highlight(true)
				hovered_bone = weakref(current_bone_selector)


func single_select_move_cursor(mouse_pos):
	point_raycast($MouseRay2, mouse_pos)
	if $MouseRay2.get_collider() != null:
		if ($MouseRay2.get_collider().get_parent().has_method("select_leaf")):
			var currentLUC = $MouseRay2.get_collider().get_parent()
			var prevLCU = null
			if leafUnderCursor:
				prevLCU = leafUnderCursor.get_ref()
			if prevLCU != currentLUC:
				if leafUnderCursor:
					if leafUnderCursor.get_ref():
						leafUnderCursor.get_ref().select_leaf(0)
				currentLUC.select_leaf(1)
				leafUnderCursor = weakref(currentLUC)
	elif leafUnderCursor:
		if leafUnderCursor.get_ref():
			leafUnderCursor.get_ref().select_leaf(0)
			leafUnderCursor = null


func switch_tool(selected_tool):
	if selected_tool == 0:
		if leafUnderCursor:
			if leafUnderCursor.get_ref():
				leafUnderCursor.get_ref().select_leaf(0)
			leafUnderCursor = null
	if selected_tool == 1:
		pass
	elif selected_tool == 2:
		pass
	$UI/ToolModeUI4/Tool0Info.visible = selected_tool == 0
	$UI/ToolModeUI4/Tool1Info.visible = selected_tool == 1
	set_transparent_leaves(selected_tool == 2)
	$Wall/CutToolLine.visible = selected_tool == 0
	$UI/ToolModeUI4/AxisButtons.visible = selected_tool == 2
	hide_hovered_bone()
	if $UI/ToolModeUI.get_child_count() > selected_tool:
		for t in $UI/ToolModeUI.get_children():
			t.modulate = transp_button_color
			tool_mode = selected_tool
			$UI/ToolModeUI.get_child(selected_tool).modulate = Color.white


func switch_axis(_axis):
	rot_axis_index = _axis
	var a_names = ['x', 'y', 'z']
	for i in 3:
		if rot_axis_index == i:
			$UI/ToolModeUI4/AxisButtons.get_node(a_names[i]).modulate = Color.white
		else:
			$UI/ToolModeUI4/AxisButtons.get_node(a_names[i]).modulate = transp_button_color


func cut_leafs(delete_all = false):
	var emit_positions = []
	for c in leafControlPoints:
		for t in c.get_children():
			if t.has_method("select_leaf"):
				if t.state == 1 or delete_all:
					var t_pos = t.global_transform.origin
					emit_positions.append(t_pos)
					t.queue_free()
	if emit_positions.size() > 0 and !delete_all:
		if tool_mode == 0:
			$Effects/Cut2.play()
		else:
			$Effects/Cut.play()
	if !delete_all:
		var emiters_num = emit_positions.size()
		if emiters_num > 5:
			emiters_num = int(sqrt(emit_positions.size()))
		for i in range(emit_positions.size()):
			if (i % emiters_num) == 0:
				var cut_effect = cut_effect_obj.instance()
				tree.get_ref().add_child(cut_effect)
				cut_effect.global_transform.origin = emit_positions[i]


func release_tool():
	$CutTool.global_transform.origin = $ToolPosition.global_transform.origin
	$Wall/AnimationPlayer.play("base")


func rotate_bone(value):
	var axis = Vector3(1.0, 0.0, 0.0) 
	if rot_axis_index == 1:
		axis = Vector3(0.0, 1.0, 0.0) 
	elif rot_axis_index == 2:
		axis = Vector3(0.0, 0.0, 1.0) 
	if hovered_bone:
		if hovered_bone.get_ref():
			var selected_bone_name = hovered_bone.get_ref().bone_name
			tree.get_ref().rotate_bone(axis, value, selected_bone_name)


func hide_hovered_bone():
	if hovered_bone:
		if hovered_bone.get_ref():
			hovered_bone.get_ref().highlight(false)
			hovered_bone = false


func save_preview_image(_name):
	$UI.visible = false
	yield(get_tree(),"idle_frame")
	yield(get_tree(),"idle_frame")
	yield(get_tree(),"idle_frame")
	var image = get_viewport().get_texture().get_data()
	image.flip_y()
	if image.get_width() >= image.get_height():
		var h = (image.get_width() - image.get_height()) / 2
		image.flip_x()
		image.crop(image.get_height() + h,image.get_height())
		image.flip_x()
		image.crop(image.get_height(),image.get_height())
	else:
		var v = (image.get_height() - image.get_width()) / 2
		image.flip_y()
		image.crop(image.get_width(), image.get_width() + v)
		image.flip_y()
		image.crop(image.get_width(), image.get_width())

	image.resize(128, 128, Image.INTERPOLATE_CUBIC)
	image.save_png("user://" + _name + ".png")
	yield(get_tree(),"idle_frame")
	$UI.visible = true


func _input(event):
	if event.is_action_pressed("scr"):
		save_preview_image("scrn")
	if event.is_action_pressed("RMB"):
		mouse_pressed = false
		release_tool()
	if (event.is_action_pressed("reset")):
		gen_tree_start()
	if (event.is_action_pressed("fire")):
		if tool_mode == 0 and !mouse_over_tools and !menu_shown:
			hit_cursor(event.position)
			mouse_pressed = true
			$Wall/AnimationPlayer.play("show")
		elif tool_mode == 1 and leafUnderCursor :
			if leafUnderCursor.get_ref():
				cut_leafs()
				leafUnderCursor = null
	elif event.is_action_released("fire"):
		if tool_mode == 0:
			if mouse_pressed:
				cut_leafs()
			mouse_pressed = false
			release_tool()
	if event is InputEventMouseMotion and !menu_shown:
		if tool_mode == 0 and mouse_pressed and !mouse_over_tools:
			move_cursor(event.position)
		elif tool_mode == 1 and !mouse_over_tools:
			single_select_move_cursor(event.position)
		elif tool_mode == 2:
			bone_select_move_cursor(event.position)
	if event.is_action_released("WUp"):
		if tool_mode == 0:
			shift_wall(w_speed)
			corretc_cut_tool_pos = true
		if tool_mode == 2:
			rotate_bone(1.0)
	elif event.is_action_released("WDown"):
		if tool_mode == 0:
			shift_wall(-w_speed)
			corretc_cut_tool_pos = true
		if tool_mode == 2:
			rotate_bone(-1.0)
	if event.is_action_released("x_axis"):
		switch_axis(0)
	if event.is_action_released("y_axis"):
		switch_axis(1)
	if event.is_action_released("z_axis"):
		switch_axis(2)
	if event.is_action_pressed("k1"):
		switch_tool(0)
	elif event.is_action_pressed("k2"):
		switch_tool(1)
	elif event.is_action_pressed("k3"):
		switch_tool(2)


func set_transparent_leaves(to_set):
	for c in leafControlPoints:
		for t in c.get_children():
			if t.has_method("set_transp"):
				t.set_transp(to_set)


func shift_wall(offset):
	var w_pos = $Wall.global_transform.origin
	w_pos.z += offset
	$Wall.global_transform.origin = w_pos
	$Wall/AnimationPlayer.stop()
	$Wall/AnimationPlayer.play("fade_in_out")


func _on_Area1_body_entered(body):
	if !mouse_pressed:
		return
	if body.get_parent().has_method("select_leaf"):
		body.get_parent().select_leaf(1)


func _on_Area1_body_exited(body):
	if body.get_parent().has_method("select_leaf"):
		body.get_parent().select_leaf(0)


func tool_button_pressed(value):
	if tool_mode == 2:
		set_transparent_leaves(false)
	elif value == 2:
		set_transparent_leaves(true)
	switch_tool(value)


func _on_ToolModeUI_mouse_entered():
	mouse_over_tools = true


func _on_ToolModeUI_mouse_exited():
		mouse_over_tools = false


func _on_GenStartTimer_timeout():
	generate_tree()


func _on_GenStopTimer_timeout():
	show_load_screen(false)


func _on_MenuButton_pressed():
	show_main_menu(!$UI/Menu.visible)


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_NewTreeButton_pressed():
	show_main_menu(false)
	yield(get_tree(),"idle_frame")
	yield(get_tree(),"idle_frame")
	gen_tree_start()


func _on_SaveButton_pressed():
	$UI/Menu.visible = false
	$UI/SaveScreen.visible = true


func _on_SaveConfButton_pressed():
	var _input = get_node("UI/SaveScreen/Panel/GridContainer/NinePatchRect/SaveFileName")
	save_preview_image(_input.text)
	Global.save_tree(_input.text, null)
	$UI/SaveScreen.visible = false
	show_main_menu(false)


func _on_SaveCencelButton_pressed():
	$UI/SaveScreen.visible = false
	show_main_menu(false)


func build_gallery(tree_list):
	var gallery_grid = $UI/GalleryScreen/ScrollContainer/GalleryGrid
	for o in gallery_grid.get_children():
		o.queue_free()
	for k in tree_list.keys():
		var gallery_item = gallery_item_obj.instance()
		gallery_grid.add_child(gallery_item)
		gallery_item.fill_in(tree_list[k], k)
	$UI/Menu.visible = false
	$UI/GalleryScreen.visible = true


func _on_LoadButton_pressed():
	var tree_list = Global.load_list()
	build_gallery(tree_list)


func _on_GalleryBackButton_pressed():
	$UI/GalleryScreen.visible = false
	show_main_menu(false)
