extends Spatial
export(Array, NodePath) var leafControlPaths
export(NodePath) var skeletonPath
export(Array, String) var ignoreBones
onready var skel = get_node(skeletonPath)
export var camera_offset = 0.0
var bones = []
var id
var bone_selector_obj = preload("res://models/tree1/BoneSelector.tscn")

func reset_pose():
	for b in range(skel.get_bone_count()):
		$tree_arm/Skeleton.set_bone_pose(b, Transform())

func _ready():
	id = skel.find_bone("a2")
	var t = skel.get_bone_pose(id)
	var tempBones = []
	for i in range(skel.get_bone_count()):
		#if skel.get_bone_parent(i) > 0:
		var to_ignore = false
		for ib in ignoreBones:
			if skel.get_bone_name(i) == ib:
				to_ignore = true
		if !to_ignore:
			tempBones.append(i)
	for i in range(tempBones.size()):
		var n = 0
		for j in range(skel.get_bone_count()):
			if skel.get_bone_parent(j) == tempBones[i]:
				n = j
		if n > 0:
			bones.append([tempBones[i], n])
	for i in range(bones.size()):
		var bone_selector = bone_selector_obj.instance()
		skel.add_child(bone_selector)
		bone_selector.bone_name = skel.get_bone_name(bones[i][0])
		bone_selector.boneID = bones[i]
		
		var bone_pos = skel.get_bone_global_pose(bones[i][0]).origin
		var child_pos = skel.get_bone_global_pose(bones[i][1]).origin
		var bone_len = bone_pos.distance_to(child_pos)
		var b_mesh = bone_selector.get_node("Mesh")
		var n_mat = b_mesh.get_surface_material(0).duplicate()
		b_mesh.set_surface_material(0, n_mat)
		
		var mesh_pos = b_mesh.transform.origin
		var newMesh = b_mesh.mesh.duplicate()
		b_mesh.mesh = newMesh
		mesh_pos.y = bone_len / 2 
		b_mesh.transform.origin = mesh_pos
		b_mesh.mesh.mid_height = bone_len / 2.0
		var b_col = bone_selector.get_node("bStatic/BoneCollider")
		var new_col_shape = b_col.get_shape().duplicate()
		b_col.shape = new_col_shape
		b_col.get_shape().height = bone_len / 2.0
		b_col.transform.origin = mesh_pos
		
	set_process(false)


func rotate_bone(axis, value, bone_name):
	var bone_ID = skel.find_bone(bone_name)
	var t = skel.get_bone_pose(bone_ID)
	t = t.rotated(axis, 0.02 * value)
	skel.set_bone_pose(bone_ID, t)


func _process(delta):
	var t = skel.get_bone_pose(id)
	t = t.rotated(Vector3(0.0, 1.0, 0.0), 0.1 * delta)
	skel.set_bone_pose(id, t)
