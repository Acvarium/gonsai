extends BoneAttachment

var boneID = -1
var is_selected = false

func select(to_select):
	if to_select:
		$AnimationPlayer.play("blink")
	else:
		$AnimationPlayer.play("base")
	is_selected = to_select
	

func highlight(to_highlught):
	if to_highlught:
		if !is_selected:
			$AnimationPlayer.play("highlight")
	else:
		if !is_selected:
			$AnimationPlayer.play("base")
		
