extends Spatial
export (NodePath) var targets_path
onready var targets_left = get_node(targets_path).get_node("left")
onready var targets_right = get_node(targets_path).get_node("right")

var target_pos = Vector3()
var speed = 0.3


func _ready():
	randomize()
	$AnimationPlayer.get_animation("ArmatureAction").loop = true
	$AnimationPlayer.play("ArmatureAction")
	random_timeout()
	
	
func _physics_process(delta):
	var new_transform = global_transform.looking_at(target_pos, Vector3.UP)
	var rand_vec = Vector3((randf() - 0.5) * 2.0, (randf() - 0.5) * 15.0, (randf() - 0.5) * 2.0)
	var rand_target_pos = target_pos + rand_vec
	new_transform.origin = rand_target_pos
	global_transform  = global_transform.interpolate_with(new_transform, speed * delta)
	var target_dist = global_transform.origin.distance_to(rand_target_pos)
	if target_dist < 1.0:
		random_timeout()


func random_timeout():
	set_physics_process(false)
	var timeout = randf() * 50.0
	$Timer.wait_time = timeout
	$Timer.start() 


func generate_path():
	set_physics_process(true)
	$AnimationPlayer.playback_speed = randf() * 0.4 + 0.8
	var start_pos = Vector3()
	var start_at_left = randf() > 0.5
	var targets_start = targets_left
	var targets_end = targets_right
	if !start_at_left:
		targets_start = targets_right
		targets_end = targets_left
	var rand_pos_obj_id = randi()%targets_start.get_child_count()
	start_pos = targets_start.get_child(rand_pos_obj_id).global_transform.origin
	var target_pos_obj_id = randi()%targets_end.get_child_count()
	target_pos = targets_end.get_child(target_pos_obj_id).global_transform.origin
	speed = start_pos.distance_to(target_pos) / 300.0
	global_transform.origin = start_pos
	global_transform = global_transform.looking_at(target_pos, Vector3.UP)


func _on_Timer_timeout():
	generate_path()
