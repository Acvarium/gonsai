extends Panel
var image_name = ""
var item_name = ""

func _ready():
	pass # Replace with function body.


func fill_in(_image_name, _item_name):
	image_name = _image_name
	item_name = _item_name
	$G/Panel/GGG/Name.text = item_name
	var image = Image.new()
	var image_path = "user://" + image_name
	var err = image.load(image_path)
	if err != OK:
		pass
	var texture = ImageTexture.new()
	texture.create_from_image(image, 0)
	$G/Panel2/PrevImage.texture = texture
