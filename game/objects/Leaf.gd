extends MeshInstance

func _ready():
	pass # Replace with function body.

var state = 0
var is_transparent = false

func set_transp(to_set):
	is_transparent = to_set
	if to_set:
		$AnimationPlayer.play("transp")
	else:
		select_leaf(state)
	

func select_leaf(sel_type = 0):
	state = sel_type
	if sel_type == 0:
		if !is_transparent:
			$AnimationPlayer.play("base")
		else:
			$AnimationPlayer.play("transp")
	elif sel_type == -1:
		$AnimationPlayer.play("red")
	elif sel_type == 1:
		$AnimationPlayer.play("blue")
		
